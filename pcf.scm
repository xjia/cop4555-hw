(define (interp e)
;;; Syntax:
;;;  e ::= x | n | true | false | succ | pred | iszero
;;;      | if e then e else e | fn x => e | e e | rec x => e
;;;      | let x = e in e
;;;
;;; * N-ary functions can be expressed by using unary functions.
  
  (define (first  x) (car x))
  (define (second x) (car (cdr x)))
  (define (third  x) (car (cdr (cdr x))))
  (define (fourth x) (car (cdr (cdr (cdr x)))))
  
  (define (error? e)
    (and (list? e)
         (eq? 'error (first e))))
  
  (define (subst e x t) ; e[t/x] -OR- e[x:=t]
    (cond ((symbol? e)
           (if (eq? e x) t e))
          ((list? e)
           (cond ((eq? 'if (first e))
                  (list 'if
                        (subst (second e) x t)
                        (subst (third  e) x t)
                        (subst (fourth e) x t)))
                 ((eq? 'fn (first e))
                  (if (eq? (second e) x)
                      e
                      (list 'fn
                            (second e)
                            (subst (third e) x t))))
                 ((eq? 'rec (first e))
                  (if (eq? (second e) x)
                      e
                      (list 'rec
                            (second e)
                            (subst (third e) x t))))
                 ((eq? 'let (first e))
                  (if (eq? (second e) x)
                      (list 'let
                            (second e)
                            (subst (third e) x t)
                            (fourth e))
                      (list 'let
                            (second e)
                            (subst (third  e) x t)
                            (subst (fourth e) x t))))
                 (else
                  (list (subst (first  e) x t)
                        (subst (second e) x t)))))
          (else e)))
  
  (define (interp-if e)
    ; (if b e1 e2)
    (define b  (interp (second e)))
    (define e1 (third  e))
    (define e2 (fourth e))
    ; if b then e1 else e2
    (cond ((eq? 'true  b) (interp e1))
          ((eq? 'false b) (interp e2))
          (else '(error "if needs bool condition"))))
  
  (define (interp-rec e)
    ; (rec x body)
    (define x    (second e))
    (define body (third  e))
    ; rec x => body
    (interp (subst body x e)))
  
  (define (interp-let e)
    ; (let x expr body)
    (define x    (second e))
    (define expr (third  e))
    (define body (fourth e))
    ; let x = expr in body
    ; transform to ((fn x => body) expr)
    (interp (list (list 'fn x body) expr)))
  
  (define (interp-app e)
    ; (e1 e2)
    (define e1 (interp (first  e)))
    (define e2 (interp (second e)))
    (cond ((error? e1) e1)
          ((error? e2) e2)
          ((eq? 'succ e1)
           (cond ((integer? e2) (+ e2 1))
                 (else '(error "succ needs int argument"))))
          ((eq? 'pred e1)
           (cond ((= 0 e2) 0)
                 ((integer? e2) (- e2 1))
                 (else '(error "pred needs int argument"))))
          ((eq? 'iszero e1)
           (cond ((= 0 e2) 'true)
                 ((integer? e2) 'false)
                 (else '(error "iszero needs int argument"))))
          ((eq? 'fn (first e1))       ; fn x => e
           (interp (subst (third e1)  ; e
                          (second e1) ; x
                          e2)))       ; v
          (else '(error "not implemented"))))
  
  (cond ((integer?    e) e)
        ((eq? 'true   e) 'true)
        ((eq? 'false  e) 'false)
        ((eq? 'succ   e) 'succ)
        ((eq? 'pred   e) 'pred)
        ((eq? 'iszero e) 'iszero)
        ((list? e)
         (cond ((eq? 'fn  (first e)) e)
               ((eq? 'if  (first e)) (interp-if  e))
               ((eq? 'rec (first e)) (interp-rec e))
               ((eq? 'let (first e)) (interp-let e))
               (else (interp-app e))))
        ((symbol? e) '(error "unbound identifier"))
        (else e)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define twice-pcf
  '(let twice (fn f (fn x (f (f x))))
     (((((twice twice) twice) twice) succ) 0)))
;;=> 65536

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define minus-pcf
  '(let minus (rec m
                (fn x (fn y (if (iszero y) x ((m (pred x)) (pred y))))))
     ((minus 125) 79)))
;;=> 46

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define factorial-pcf
  '(let plus (rec p
               (fn x (fn y (if (iszero x) y ((p (pred x)) (succ y))))))
     (let times (rec t
                  (fn x (fn y (if (iszero x) 0 ((plus y) ((t (pred x)) y))))))
       (let factorial (rec f
                        (fn n (if (iszero n) 1 ((times n) (f (pred n))))))
         (factorial 6)))))
;;=> 720

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define fibonacci-pcf
  '(let plus (rec p
               (fn x (fn y (if (iszero x) y ((p (pred x)) (succ y))))))
     (let fibonacci (rec f
                      (fn n (if (iszero n)
                                0
                                (if (iszero (pred n))
                                    1
                                    ((plus (f (pred n))) (f (pred (pred n))))))))
       (fibonacci 20))))
;;=> 6765

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Here we show that, quite surprisingly, we can implement *lists* in PCF
; by using *first-class functions*.  The basic idea is that a list is
; represented by a function f, where f 0 returns the head, f 1 returns
; the tail, and f 2 returns true if the list is empty and false otherwise.
(define lists-pcf
  '(let cons (fn x (fn xs (fn n
               (if (iszero n)
                   x
                   (if (iszero (pred n))
                       xs
                       false)))))
     (let nil (fn n true)   ; This is flawed; hd nil and tl nil both return true!
       (let hd (fn f (f 0))
         (let tl (fn f (f 1))
           (let null (fn f (f 2))
             (let equal ; This tests whether two integers are equal.
               (rec e (fn a (fn b
                 (if (iszero a)
                     (iszero b)
                     (if (iszero b)
                         false
                         ((e (pred a)) (pred b)))))))
               (let member
                 (rec m (fn n (fn ns
                   (if (null ns)
                       false
                       (if ((equal n) (hd ns))
                           true
                           ((m n) (tl ns)))))))
                 ((member 4) ; Test whether 4 is a member of the list.
                  ((cons 1)
                   ((cons 2)
                    ((cons 3)
                     ((cons 4)
                      ((cons 5) nil))))))))))))))
;;=> true

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(interp twice-pcf)
(interp minus-pcf)
(interp factorial-pcf)
(interp fibonacci-pcf)
(interp lists-pcf)
