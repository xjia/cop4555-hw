(*  Here's a skeleton file to help you get started on Interpreter 1.  *)

use "parser.sml";

(*  Here you need to put in the definition of
      subst : term -> string -> term -> term
*)

fun subst (AST_ID s) x t = if s=x then t else (AST_ID s)
  | subst (AST_IF (b, e1, e2)) x t =
      AST_IF (subst b x t, subst e1 x t, subst e2 x t)
  | subst (AST_APP (e1, e2)) x t =
      AST_APP (subst e1 x t, subst e2 x t)
  | subst (AST_FUN (s, e)) x t =
      if s=x then AST_FUN (s, e)
             else AST_FUN (s, subst e x t)
  | subst (AST_REC (s, e)) x t =
      if s=x then AST_REC (s, e)
             else AST_REC (s, subst e x t)
  | subst e _ _ = e

(*  Here's a partial skeleton of interp : term -> term.
    Also, I've shown you how rule (7) can be implemented.
*)

fun interp (AST_ID s) = AST_ERROR ("unbound identifier: " ^ s)
  | interp (AST_IF (b, e1, e2)) =
      (case (interp b) of
          (AST_ERROR s)     => AST_ERROR s
        | (AST_BOOL true)   => interp e1
        | (AST_BOOL false)  => interp e2
        | (_)               => AST_ERROR "if needs bool condition")
  | interp (AST_APP (e1, e2)) =
      (case (interp e1, interp e2) of
          (AST_ERROR s, _)        => AST_ERROR s
        | (_, AST_ERROR s)        => AST_ERROR s
        | (AST_SUCC, AST_NUM n)   => AST_NUM (n+1)
        | (AST_SUCC, _)           => AST_ERROR "succ needs int argument"
        | (AST_PRED, AST_NUM 0)   => AST_NUM 0
        | (AST_PRED, AST_NUM n)   => AST_NUM (n-1)
        | (AST_PRED, _)           => AST_ERROR "pred needs int argument"
        | (AST_ISZERO, AST_NUM 0) => AST_BOOL true
        | (AST_ISZERO, AST_NUM _) => AST_BOOL false
        | (AST_ISZERO, _)         => AST_ERROR "iszero needs int argument"
        | (AST_FUN (x, e), v1)    => interp (subst e x v1)
        | (_, _) => AST_ERROR "other function applications not implemented")
  | interp (AST_FUN (x, e)) = AST_FUN (x, e)
  | interp (AST_REC (x, e)) = interp (subst e x (AST_REC (x, e)))
  | interp e = e

(*  Once you have defined interp, you can try out simple examples by
      interp (parsestr "succ (succ 7)");
    and you can try out larger examples by
      interp (parsefile "factorial.pcf");
*)

